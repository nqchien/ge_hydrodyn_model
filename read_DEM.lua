#!/usr/bin/env lua

-- read an ASCII bathymetry file from command prompt, (default: cairns.asc)
-- write a KML file denoting boundaries of the data, and the grid (default: grid.kml)
-- usage:  lua read_bathy.lua  infile  outfile.kml

fn_cairns = "cairns.asc"
fn_kml = "grid.kml"
CENTER_MERIDIAN = 147       -- zone 55
FALSE_EASTING = 500000
FALSE_NORTHING = 10000000

SPACING = 20   -- to coarsen the grid when display

local f_cairns = assert(io.open(fn_cairns, "r"))
local f_kml = assert(io.open(fn_kml, "w"))

ncols = tonumber(string.match( f_cairns:read("*line"), "%a+%s+(%d+)"))
nrows = tonumber(string.match( f_cairns:read("*line"), "%a+%s+(%d+)"))

xLLcorner = tonumber(string.match( f_cairns:read("*line"), "%a+%s+(%d*%.*%d*)"))
yLLcorner = tonumber(string.match( f_cairns:read("*line"), "%a+%s+(%d*%.*%d*)"))
cellSize = tonumber(string.match( f_cairns:read("*line"), "%a+%s+(%d*%.*%d*)"))

NODATA_value = tonumber(string.match( f_cairns:read("*line"), "%a+%s+(%d*%.*%d*)"))

f_cairns:close()

-- calculate long / lat
xW = xLLcorner
xE = xW + ncols * cellSize
yS = yLLcorner
yN = yS + nrows * cellSize

require( 'conversion' )
longSW , latSW = long_lat_from_UTM( xW - FALSE_EASTING, yS - FALSE_NORTHING, CENTER_MERIDIAN )
longSE , latSE = long_lat_from_UTM( xE - FALSE_EASTING, yS - FALSE_NORTHING, CENTER_MERIDIAN )
longNE , latNE = long_lat_from_UTM( xE - FALSE_EASTING, yN - FALSE_NORTHING, CENTER_MERIDIAN )
longNW , latNW = long_lat_from_UTM( xW - FALSE_EASTING, yN - FALSE_NORTHING, CENTER_MERIDIAN )

-- difference in long/lat between two consecutive grid lines
delta_lat = (latNW - latSW) / nrows
-- difference in long depends on the lats themselves
delta_longS = (longSE - longSW) / ncols
delta_longN = (longNE - longNW) / ncols

-- templates for output KML file
kml_header = [[<?xml version="1.0" encoding="utf-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Folder>
    <name>DEM</name>
    <open>0</open>
    <description>Digital Elevation Model data</description>]]

kml_DEM_extent = [[
    <Placemark id="DEM_extent">
        <name>DEM extent</name>
        <description />
        <Style>
            <LineStyle>
                <color>%x</color> 
                <width>%f</width>
            </LineStyle>
            <PolyStyle>
                <color>%x</color>
            </PolyStyle>
        </Style>
            <Polygon>
                <extrude>0</extrude>
                <tessellate>0</tessellate>
                <altitudeMode>clampToGround</altitudeMode>
                <outerBoundaryIs>
                    <LinearRing>
                        <coordinates>
                            %f, %f 
                            %f, %f 
                            %f, %f 
                            %f, %f 
                            %f, %f 
                        </coordinates>
                    </LinearRing>
                </outerBoundaryIs>
            </Polygon>
    </Placemark>]]

kml_DEM_grid_header = [[
    <Placemark>
        <Style>
            <LineStyle>
                <color>%x</color> 
                <width>%d</width>
            </LineStyle>
        </Style>
        <MultiGeometry>
]]

kml_DEM_grid = [[
            <LineString>
                <extrude>0</extrude>
                <tessellate>0</tessellate>
                <altitudeMode>clampToGround</altitudeMode>
                <coordinates>%f, %f  %f, %f </coordinates>
            </LineString>
]]

kml_DEM_grid_footer = [[
        </MultiGeometry>
    </Placemark>
]]

kml_footer = [[
  </Folder>
</kml>
]]

f_kml:write( kml_header )
f_kml:write( string.format( kml_DEM_extent, 0xff00ffff, 3, 0x4000ffff, longSW, latSW, 
        longSE, latSE, longNE, latNE, longNW, latNW, longSW, latSW) )

f_kml:write( string.format( kml_DEM_grid_header, 0xff0080ff, 1 ) )

-- generate each grid line
for i = 0, ncols do
    if i % SPACING == 0 then
        longiS = longSW + i * delta_longS
        longiN = longNW + i * delta_longN
        f_kml:write( string.format( kml_DEM_grid, longiS, latSW, longiN, latNW ))
    end
end
for j = 0, nrows do
    if j % SPACING == 0 then
        latj = latSW + j * delta_lat
        f_kml:write( string.format( kml_DEM_grid, longSW, latj, longSE, latj ))
    end
end

f_kml:write( kml_DEM_grid_footer )
f_kml:write( kml_footer )
f_kml:close( )
