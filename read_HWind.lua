#!/usr/bin/env lua

-- read a file from command prompt, or AL092008_0913_1630 (default)
-- usage:  lua read_HWind.lua  infile  outfile.kml

local SIZE = 13      -- the number of character bytes for each number in data file

fn_hwind = arg[1] or "AL092008_0913_1630"
fn_kml = arg[2] or "outfile.kml"

local f_hwind = assert(io.open(fn_hwind, "r"))
local f_kml = assert(io.open(fn_kml, "w"))

title = f_hwind:read("*line")
line = f_hwind:read("*line")

cellSize = tonumber(string.match( line, "%a+=%a+=%s(%d*%.*%d*)"))

line = f_hwind:read("*line")
centerLong = tonumber(string.match( line, "(-*%d*%.*%d*)%s*EAST "))
centerLat = tonumber(string.match( line, "(-*%d*%.*%d*)%s*NORTH "))

x = {}
y = {}

f_hwind:read("*line")
lenDataX = f_hwind:read("*n")

for i = 1,lenDataX do
    x[i] = f_hwind:read("*n")
end
f_hwind:read("*line")

f_hwind:read("*line")
lenDataY = f_hwind:read("*n")

for i = 1,lenDataY do
    y[i] = f_hwind:read("*n")
end
f_hwind:read("*line")

-- read long, lat
lambda = {}
phi = {}
f_hwind:read("*line")
if f_hwind:read("*n") ~= lenDataX then error("Inconsistent data size") end

for i = 1,lenDataX do
    lambda[i] = f_hwind:read("*n")
end
f_hwind:read("*line")

f_hwind:read("*line")
if f_hwind:read("*n") ~= lenDataY then error("Inconsistent data size") end

for i = 1,lenDataY do
    phi[i] = f_hwind:read("*n")
end
f_hwind:read("*line")

-- read u, v
u = {}
v = {}
f_hwind:read("*line")
if f_hwind:read("*n") ~= lenDataX then error("Inconsistent data size") end
if f_hwind:read("*n") ~= lenDataY then error("Inconsistent data size") end
f_hwind:read("*line")

nlines = math.floor(lenDataX / 2)
uvpattern = "%(%s*(-*%d+%.*%d*)%,%s*(-*%d+%.*%d*)%s*%)"
for j = 1,lenDataY do
    
    for i = 1, nlines do
        ustr1, vstr1, ustr2, vstr2 = (f_hwind:read("*line")):match(uvpattern .. uvpattern)
        u[(j - 1) * lenDataX + i * 2 - 1] = tonumber(ustr1)
        u[(j - 1) * lenDataX + i * 2] = tonumber(ustr2)
        v[(j - 1) * lenDataX + i * 2 - 1] = tonumber(vstr1)
        v[(j - 1) * lenDataX + i * 2] = tonumber(vstr2)
    end
    
    if lenDataX > 2 * nlines then   -- one remaining data to be read
        ustr1, vstr1 = (f_hwind:read("*line")):match(uvpattern)
        u[j * lenDataX] = tonumber(ustr1)
        v[j * lenDataX] = tonumber(vstr1)
    end
end
print(#x, #y, #lambda, #phi, #u, #v)

f_hwind:close()

-- write to KML
-- vector field comprises of various arrows, each is a GroundOverlay
-- using image file arrow.gif

kml_header = [[
<?xml version="1.0" encoding="utf-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <name>Wind Vector Overlay</name>
    <open>0</open>
    <description>Test with Hwind data</description>
]]

kml_arrow_header = [[
    <Placemark>
        <Style>
            <LineStyle>
                <color>%x</color> 
                <width>%d</width>
            </LineStyle>
        </Style>
        <MultiGeometry>
]]

kml_arrow = [[
            <LineString>
                <extrude>0</extrude>
                <tessellate>0</tessellate>
                <altitudeMode>clampToGround</altitudeMode>
                <coordinates>%f, %f  %f, %f  %f, %f  %f, %f  %f, %f</coordinates>
            </LineString>
]]

kml_arrow_footer = [[
        </MultiGeometry>
    </Placemark>
]]

kml_footer = [[
  </Document>
</kml>
]]

-- find the maximum length component
max = 0
for k = 1, #u do
    u[k] = u[k] or 0
    v[k] = v[k] or 0
    if u[k] > max then max = u[k] end
    if v[k] > max then max = v[k] end
end

f_kml:write(kml_header)
f_kml:write(string.format(kml_arrow_header, 0xff00ffff, 2))

--[[
color specifications:
0xff00ffff  : Aqua
0xffed9564  : Cornflower blue
0xff8b0000  : DarkBlue
0xff006400 : DarkGreen
0xff008cff  : DarkOrange
--]]

sqrt = math.sqrt
sin = math.sin
cos = math.cos
asin = math.asin
atan2 = math.atan2
pi = math.pi

sind = function(x) return sin(x * pi / 180) end
cosd = function(x) return cos(x * pi / 180) end
asind = function(x) return asin(x) * 180 / pi end
atand2 = function(y,x) return atan2(y,x) * 180/pi end

-- Calculate new long and lat based on long and lat of original point (lon1, lat1)
-- the bearing angle is w.r.t N. Clockwise angle is positive.

newcoords = function(lon1, lat1, dist, brng)
    local R = 6371          -- average earth radius in km
    lat2 = asind(sind(lat1) * cos(dist/R) + cosd(lat1) * sin(dist/R)* cosd(brng))
    lon2 = lon1 + atand2(sind(brng) * sin(dist/R) * cosd(lat1), cos(dist/R) - sind(lat1) * sind(lat2))
    return lon2, lat2
end

LEN_COEF = 1.5              -- coefficient of arrow length = ratio longest arrow / grid size
ARROW_ANGLE = 20         -- angle between arrow trunk and either arrow head
ARROW_HEAD_LEN = 0.33  -- ratio of arrow head to length of entire arrow
SPACING = 2                     -- if the array is to dense, only draw once for each multiple of rows/ cols

turningAngle = 180 - ARROW_ANGLE

wsfile = assert(io.open('w_speed.txt', 'w'))

for j = 1, lenDataY do        -- lenDataY
    for i = 1, lenDataX do    -- lenDataX
        k = (j - 1) * lenDataX + i
        windSpeed = sqrt(u[k]^2 + v[k]^2)
        scaleFactor = windSpeed / max
        L = cellSize * scaleFactor * SPACING    -- arrow length or vector magnitude
        ang = 90 - atand2(v[k], u[k])    -- bearing angle
        arrowheadLon, arrowheadLat = newcoords(lambda[i], phi[j], L, ang)
        arrowleftLon, arrowleftLat = newcoords(arrowheadLon, arrowheadLat, L * ARROW_HEAD_LEN, ang - turningAngle)
        arrowrightLon, arrowrightLat = newcoords(arrowheadLon, arrowheadLat, L * ARROW_HEAD_LEN, ang + turningAngle)
        if (j % SPACING == 0) and (i % SPACING == 0) then 
            f_kml:write(string.format(kml_arrow, lambda[i], phi[j],
                    arrowheadLon, arrowheadLat, arrowleftLon, arrowleftLat, 
                    arrowheadLon, arrowheadLat, arrowrightLon, arrowrightLat))
        end
        wsfile:write(windSpeed .. "\n")
    end
end

wsfile:close()

f_kml:write( kml_arrow_footer )
f_kml:write( kml_footer )

f_kml:close( )

-- zip it as kmz
fn_kmz = fn_kml:gsub("kml", "kmz")
os.execute("zip " .. fn_kmz .. " " .. fn_kml)
