#!/usr/bin/env lua

-- read an ASCII bathymetry file from command prompt, (default: cairns.asc)
-- write a KML file denoting boundaries of the data, and the grid (default: grid.kml)
-- usage:  lua read_bathy.lua  infile  outfile.kml

CENTER_MERIDIAN = 147       -- zone 55
FALSE_EASTING = 500000
FALSE_NORTHING = 10000000

SPACING = 20   -- to coarsen the grid when display

kml_header = [[<?xml version="1.0" encoding="utf-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Folder>
    <name>Cairn modeling regions</name>
    <open>0</open>
    <description>Regions in model of Cairns test case.</description>
]]
kml_region_header = [[
    <Placemark id="%s">
        <name>%s</name>
        <description />
        <Style>
            <LineStyle>
                <color>%x</color> 
                <width>%f</width>
            </LineStyle>
            <PolyStyle>
                <color>%x</color>
            </PolyStyle>
        </Style>
            <Polygon>
                <extrude>0</extrude>
                <tessellate>0</tessellate>
                <altitudeMode>clampToGround</altitudeMode>
                <outerBoundaryIs>
                    <LinearRing>
                        <coordinates>
]]
kml_region_footer = [[
                        </coordinates>
                    </LinearRing>
                </outerBoundaryIs>
            </Polygon>
    </Placemark>
]]

kml_footer = [[
  </Folder>
</kml>
]]

require( 'conversion' )
fnames = {'islands.csv', 'islands1.csv', 'islands2.csv', 'islands3.csv', 'extent.csv',
'shallow.csv', 'cairns.csv'}

fillcolors = {0x668b008b, 0x668b008b, 0x668b008b, 0x668b008b, 0x002fffad, 0x00cfffff, 0xff0099ff}
linecolors = {0xff8b008b, 0xff8b008b, 0xff8b008b, 0xff8b008b, 0xff2fffad, 0xffcfffff, 0xff0099ff}
fn_kml = 'regions.kml'

local f_kml = assert(io.open(fn_kml, "w"))

f_kml:write(kml_header)
for i = 1, #fnames do
    local fi = assert( io.open(fnames[i], 'r') )
    f_kml:write( string.format( kml_region_header, i, fnames[i], linecolors[i], 3, fillcolors[i] ))
    count = 0
    while true do
        count = count + 1
        line = fi:read( )
        if line == nil then break end
        xp = tonumber(string.match( line, "(%d*%.*%d*).*"))
        yp = tonumber(string.match( line, ".*,(%d*%.*%d*)"))
        lon, lat = long_lat_from_UTM( xp - FALSE_EASTING, yp - FALSE_NORTHING, CENTER_MERIDIAN )
        if count == 1 then
            lonsave = lon
            latsave = lat
        end
        f_kml:write( string.format( "%f,%f\n", lon, lat) )
    end
    f_kml:write( string.format( "%f,%f\n", lonsave, latsave) )
    f_kml:write( kml_region_footer )
    fi:close( )
end

f_kml:write( kml_footer )
