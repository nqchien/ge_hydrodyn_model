kml_header = """<?xml version="1.0" encoding="utf-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Folder>
    <name>Wind Contour Overlay</name>
    <open>0</open>
    <description>Contours of wind speed in m/s.</description>"""

kml_contour = """
    <Placemark id="%d">
        <name>%s</name>
        <description>  </description>
        <Style>
            <LineStyle>
                <color>%x</color> 
                <width>%f</width>
            </LineStyle>
        </Style>
            <LineString>
                <extrude>0</extrude>
                <tessellate>0</tessellate>
                <altitudeMode>clampToGround</altitudeMode>
                <coordinates>%s</coordinates>
            </LineString>
    </Placemark>"""

kml_pcolor = """
    <GroundOverlay>
        <name>Pseudocolor image of wind speed.</name>
        <description>  </description>
        <Icon>
            <href>%s</href>
        </Icon>
        <LatLonBox>
            <north>%f</north>
            <south>%f</south>
            <east>%f</east>
            <west>%f</west>
        </LatLonBox>
    </GroundOverlay>
"""

kml_footer = """
  </Folder>
</kml>
"""

LEN_X = 163
LEN_Y = 163

W_LONG = -99.6925
S_LAT = 26.9565

DELTA_LONG = DELTA_LAT = 0.0542

E_LONG = W_LONG + LEN_X * DELTA_LONG
N_LAT = S_LAT + LEN_Y * DELTA_LAT

RED = 0xff0000ff
YELLOW = 0xff00ffff

try:
    import numpy, pylab
except:
    print """ This code requires numpy and pylab to run
    Please download corresponding libraries from
    http://numpy.sourceforge.net
    http://matplotlib.sourceforge.net
    """

longs = numpy.arange(-99.6925, -90.9075, 0.0542)
lats = numpy.arange(26.9565, 35.7415, 0.0542)

Long, Lat = numpy.meshgrid(longs, lats)

f_kml = open("contour01.kml", "w")
f_kml.write(kml_header)

f_ws = open("w_speed.txt", "r")
L = [ float(val) for val in f_ws.read().split() ]
windSpeedArray = numpy.reshape(L, (LEN_X, LEN_Y))

contourInfo = pylab.contour(Long, Lat, windSpeedArray)

contours = contourInfo.collections
# theLabels = pylab.clabel(contourInfo, inline=1)
Cval = contourInfo.cvalues

idx = 0
for contour in contours:
    contourLine = contour.get_paths()
    idx += 1
    for contourSegment in contourLine:
        contourShape = contourSegment.to_polygons()
        npoints = numpy.shape(contourShape)[1]
        contourString = ""
        for i in xrange(npoints):
            coords = str(contourShape[0][i][0]) + "," + str(contourShape[0][i][1]) + " "
            contourString += coords
        
        segcolor = YELLOW + (RED - YELLOW) * (Cval[idx-1] - Cval[0]) / (Cval[-1] - Cval[0])
        f_kml.write(kml_contour % (Cval[idx-1], "v = " + str(Cval[idx-1]) + " m/s", 
                segcolor, 3, contourString) )

f_ws.close()

thePColor = pylab.pcolor(Long, Lat, windSpeedArray)
pcolor_fn = 'wind_speed.png'
# pylab.savefig(pcolor_fn, transparency=0.5)
f_kml.write(kml_pcolor %(pcolor_fn, N_LAT, S_LAT, E_LONG, W_LONG))

f_kml.write(kml_footer)

f_kml.close()

pylab.show()