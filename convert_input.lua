#!/usr/bin/env lua
require("conversion")

-- read the KML file
-- produce input files for AnuGA, including CSV files of polygon coordinates
-- and two Python script files: project.py and runcairns.py

fn_regions = "regions.kml"
fn_locations = "locations.kml"
fn_project = "project.py"
fn_driver = "runcairns.py"
MID_LONG = 147      -- the central meridian of UTM Zone 55K where Cairns, QLD lies within
FALSE_E = 500000        -- false Easting
FALSE_N = 10000000      -- false Northing

local f_regions = assert(io.open(fn_regions, "r"))
local f_locations = assert(io.open(fn_locations, "r"))
local f_project = assert(io.open(fn_project, "w"))
local f_driver = assert(io.open(fn_driver, "w"))

-- Read KML file (regions.kml)

-- read the first two lines (KML header)
f_regions:read()
f_regions:read()

-- parsing
-- extract data for polygons
polygons = {}
repeat
    line = f_regions:read()
    if line:find("<Placemark") ~= nil then  -- line contains this string
        content = ""
        repeat
            line = f_regions:read()
            content = content .. line .. "\n"
        until line:find("</Placemark>") ~= nil
        -- Now parsing the content obtained
        -- extract data for one polygon
        _, _, polyname = content:find("<name>([%w_]+)</name>")
        _, _, polyresolution = content:find("RES%s-=%s-(%d+).*</description>")
        _, _, strcoords = content:find("<coordinates>(.-)</coordinates>")
        polycoords = {}
        for lon, lat, alt in string.gfind(strcoords, "([+-]?[%d%.]+),([+-]?[%d%.]+),([+-]?[%d%.]+)") do
            table.insert(polycoords, {lon=tonumber(lon), lat=tonumber(lat), alt=tonumber(alt)})
        end
        lname = polyname:lower()
        if string.find(lname, "extent") ~= nil or string.find(lname, "domain") ~= nil then
            -- for the modeling domain region
            _, _, initstage = content:find("STAGE%s-=%s-([+-]?[%d%.]+).*</description>")
            _, _, initfriction = content:find("FRICTION%s-=%s-([%d%.]+).*</description>")
            _, _, remainder_res = content:find("RES%s-=%s-(%d+).*</description>")
            _, _, n_boundary = content:find("N_BOUNDARY%s-=%s-&quot;([%w%s]+)&quot;.*</description>")
            _, _, e_boundary = content:find("E_BOUNDARY%s-=%s-&quot;([%w%s]+)&quot;.*</description>")
            _, _, s_boundary = content:find("S_BOUNDARY%s-=%s-&quot;([%w%s]+)&quot;.*</description>")
            _, _, w_boundary = content:find("W_BOUNDARY%s-=%s-&quot;([%w%s]+)&quot;.*</description>")
            _, _, n_bc_type, n_bc = content:find("N_BND_CONDITION%s-=%s-{(.-),(.-)}.*</description>")
            _, _, e_bc_type, e_bc = content:find("E_BND_CONDITION%s-=%s-{(.-),(.-)}.*</description>")
            _, _, s_bc_type, s_bc = content:find("S_BND_CONDITION%s-=%s-{(.-),(.-)}.*</description>")
            _, _, w_bc_type, w_bc = content:find("W_BND_CONDITION%s-=%s-{(.-),(.-)}.*</description>")
            bounding_polygon_name = polyname
        elseif string.find(lname, "output") ~= nil then
            -- for the output region
            output_polygon_name = polyname
            lon_W = 999   lon_E = -999
            lat_S = 999   lat_N = -999
            for i, coords in ipairs(polycoords) do
                if coords.lon < lon_W then lon_W = coords.lon; lat_W = coords.lat end
                if coords.lon > lon_E then lon_E = coords.lon; lat_E = coords.lat end
                if coords.lat < lat_S then lat_S = coords.lat; lon_S = coords.lon end
                if coords.lat > lat_N then lat_N = coords.lat; lon_N = coords.lon end
            end
        else
            -- for an ordinary polygon
            table.insert(polygons, {name=polyname, res=polyresolution, coords=polycoords})
        end
    end
until line:find("</kml>") ~= nil
f_regions:close( )


f_locations:read()
f_locations:read()
gauges = {}
repeat
    line = f_locations:read()
    if line:find("<Placemark") ~= nil then  -- line contains this string
        content = ""
        repeat
            line = f_locations:read()
            content = content .. line .. "\n"
        until line:find("</Placemark>") ~= nil
        _, _, gaugename = content:find("<name>([%w%s%-]+)</name>")
        _, _, gaugeelev = content:find("ELEV%s-=%s-([+-]?%d+)</description>")
        _, _, gaugelon, gaugelat, _ = content:find(
                "<coordinates>([%-%d%.]+),([%-%d%.]+),([%-%d%.]+)</coordinates>")
        x, y = UTM_from_long_lat(gaugelon, gaugelat, MID_LONG)
        x = x + FALSE_E
        y = y + FALSE_N
        table.insert(gauges, {name=gaugename, easting=x, northing=y, elev=gaugeelev})
    end
until line:find("</kml>") ~= nil
f_locations:close( )

-- Write to the CSV file containing gauge locations
if #gauges > 0 then
    local f_gauges = assert(io.open("gauges.csv", "w"))
    f_gauges:write("easting,northing,name,elevation\n")
    for i,gauge in ipairs(gauges) do
        f_gauges:write(string.format("%.2f,%.2f,%s,%s\n", gauge.easting, gauge.northing, gauge.name, gauge.elev))
    end
    f_gauges:close()
end

-- minimum bounding rectangle for the output
xW, yW = UTM_from_long_lat(lon_W, lat_W, MID_LONG)
xE, yE = UTM_from_long_lat(lon_E, lat_E, MID_LONG)
xS, yS = UTM_from_long_lat(lon_S, lat_S, MID_LONG)
xN, yN = UTM_from_long_lat(lon_N, lat_N, MID_LONG)
xW = xW + FALSE_E       yW = yW + FALSE_N
xE = xE + FALSE_E       yE = yE + FALSE_N
xS = xS + FALSE_E       yS = yS + FALSE_N
xN = xN + FALSE_E       yN = yN + FALSE_N

eastingmax = 418000
northingmin = 8026600
northingmax = 8145700

gauge_filename = "gauges.csv"

-- Write input files
project_file_template1 = [[
# -*- coding: cp1252 -*-
"""Common filenames and locations for topographic data, meshes and outputs."""

from os import sep, environ, getenv, getcwd
from os.path import expanduser
import sys

###############################
# Domain definitions
###############################

# bounding polygon for study area
bounding_polygon = read_polygon('%s.csv')

###############################
# Interior region definitions
###############################

# interior polygons
]]
f_project:write(string.format(project_file_template1, bounding_polygon_name))

for i, polygon in ipairs(polygons) do
    name = polygon.name
    res = polygon.res
    f_project:write(string.format("poly_%s = read_polygon('%s.csv')\n", name, name))
end

project_file_template2 = [[
###################################################################
# Clipping regions for export to asc and regions for clipping data
###################################################################

# exporting asc grid
eastingmin = %f
eastingmax = %f
northingmin = %f
northingmax = %f

gauge_filename = '%s'
]]

f_project:write(string.format(project_file_template2, xW, xE, yS, yN, gauge_filename))

f_project:close( )


-- Now produce the driver file

driver_file_template = [[
# Standard modules
import os
import time
import sys

# Related major packages
from anuga.shallow_water import Domain
from anuga.shallow_water import Reflective_boundary
from anuga.shallow_water import Dirichlet_boundary
from anuga.shallow_water import Time_boundary
from anuga.shallow_water import File_boundary
from anuga.shallow_water import Transmissive_Momentum_Set_Stage_boundary
from anuga.pmesh.mesh_interface import create_mesh_from_regions
from anuga.shallow_water.data_manager import convert_dem_from_ascii2netcdf
from anuga.shallow_water.data_manager import dem2pts

# Application specific imports
import project                 # Definition of file names and polygons
from math import *

if os.access("slide", os.F_OK) == 0:
    os.mkdir("slide")
basename = "slidesource"

#------------------------------------------------------------------------------
# Preparation of topographic data
# Convert ASC 2 DEM 2 PTS using source data and store result in source data
#------------------------------------------------------------------------------

# Filenames
dem_name = 'cairns' 
meshname = 'cairns.msh'

# Create DEM from asc data
convert_dem_from_ascii2netcdf(dem_name, use_cache=True, verbose=True)

# Create pts file for onshore DEM
dem2pts(dem_name, use_cache=True, verbose=True)


# resolution requirement inform of  domain_res = RES
%s

# list of interior regions
%s

create_mesh_from_regions(project.bounding_polygon,
        boundary_tags={
                '%s': [0],
                '%s': [1],
                '%s': [2],
                '%s': [3]},
        maximum_triangle_area=%f,
        filename=meshname,
        interior_regions=interior_regions,
        use_cache=False,
        verbose=True)

#------------------------------------------------------------------------------
# Setup computational domain
#------------------------------------------------------------------------------
from anuga.abstract_2d_finite_volumes.pmesh2domain import pmesh_to_domain_instance
from anuga.caching import cache

domain = cache(pmesh_to_domain_instance,
        (meshname, Domain),
        dependencies = [meshname])

print 'Number of triangles = ', len(domain)
print 'The extent is ', domain.get_extent()
print domain.statistics()

domain.set_name(basename) 
domain.set_datadir(scenario)
domain.set_quantities_to_be_stored(['stage', 'xmomentum', 'ymomentum'])
domain.set_minimum_storable_height(0.01)

domain.tight_slope_limiters = 0
print 'domain.tight_slope_limiters', domain.tight_slope_limiters

domain.points_file_block_line_size = 50000

#------------------------------------------------------------------------------
# Setup initial conditions
#------------------------------------------------------------------------------

%s

domain.set_quantity('elevation', 
        filename=dem_name + '.pts',
        use_cache=True,
        verbose=True,
        alpha=0.1)

#------------------------------------------------------------------------------
# Setup information for slide scenario (to be applied 1 min into simulation
#------------------------------------------------------------------------------

# Function for submarine slide
from anuga.shallow_water.smf import slide_tsunami  
tsunami_source = slide_tsunami(
        length=35000.0,
        depth=project.slide_depth,
        slope=6.0,
        thickness=500.0, 
        x0=project.slide_origin[0], 
        y0=project.slide_origin[1], 
        alpha=0.0, 
        domain=domain,
        verbose=True)

#------------------------------------------------------------------------------
# Setup boundary conditions
#------------------------------------------------------------------------------

domain.set_boundary(
        {'%s': %s,
        '%s': %s,
        '%s': %s,
        '%s': %s})

#------------------------------------------------------------------------------
# Evolve system through time
#------------------------------------------------------------------------------

import time
t0 = time.time()

from Numeric import allclose
from anuga.abstract_2d_finite_volumes.quantity import Quantity

for t in domain.evolve(yieldstep = 10, finaltime = 60): 
    domain.write_time()
    domain.write_boundary_statistics(tags = 'ocean_east')     
    
# add slide
thisstagestep = domain.get_quantity('stage')
if allclose(t, 60):
    slide = Quantity(domain)
    slide.set_values(tsunami_source)
    domain.set_quantity('stage', slide + thisstagestep)

for t in domain.evolve(yieldstep = 50, finaltime = 2000, 
                       skip_initial_step = True):
# for t in domain.evolve(yieldstep = 10, finaltime = 5000, 
                       # skip_initial_step = True):
    domain.write_time()
    domain.write_boundary_statistics(tags = 'ocean_east')
]]

str_resolution = ""
str_interior_region = "interior_regions = [\n"
for i, polygon in ipairs(polygons) do
    if polygon.res ~= nil then
        name = polygon.name
        res = polygon.res
        str_resolution = str_resolution .. string.format("%s_res = %s\n", name, tostring(res))
        str_interior_region = str_interior_region .. string.format("[project.poly_%s, %s_res],\n", name, name)
    end
end
str_interior_region = str_interior_region .. "]"

qty_dict = {{'stage', initstage}, {'friction', initfriction}}
str_qty = ""

for id, item in ipairs(qty_dict) do
    if item[2] ~= nil then
        str_qty = str_qty .. string.format("domain.set_quantity('%s', %s)\n", item[1], item[2])
    end
end

str_n_bc = ''
str_e_bc = ''
str_s_bc = ''
str_w_bc = ''

if string.find(n_bc_type:lower(), "reflective") ~= nil then
    str_n_bc = "Reflective_boundary(domain)"
elseif string.find(n_bc_type:lower(), "dirichlet") ~= nil then
    str_n_bc = "Dirichlet_boundary([".. n_bc .."])"
elseif string.find(n_bc_type:lower(), "transmissive_momentum_set_stage") ~= nil then
    str_n_bc = "Transmissive_Momentum_Set_Stage_boundary(domain=domain, function=lambda t: ["
        .. n_bc .. "])"
end

if string.find(e_bc_type:lower(), "reflective") then
    str_e_bc = "Reflective_boundary(domain)"
elseif string.find(e_bc_type:lower(), "dirichlet") ~= nil then
    str_e_bc = "Dirichlet_boundary([".. e_bc .."])"
elseif string.find(e_bc_type:lower(), "transmissive_momentum_set_stage") ~= nil then
    str_e_bc = "Transmissive_Momentum_Set_Stage_boundary(domain=domain, function=lambda t: ["
        .. e_bc .. "])"
end

if string.find(s_bc_type:lower(), "reflective") then
    str_s_bc = "Reflective_boundary(domain)"
elseif string.find(s_bc_type:lower(), "dirichlet") ~= nil then
    str_s_bc = "Dirichlet_boundary(".. s_bc ..")"
elseif string.find(s_bc_type:lower(), "transmissive_momentum_set_stage") ~= nil then
    str_s_bc = "Transmissive_Momentum_Set_Stage_boundary(domain=domain, function=lambda t: ["
        .. s_bc .. "])"
end

if string.find(w_bc_type:lower(), "reflective") then
    str_w_bc = "Reflective_boundary(domain)"
elseif string.find(w_bc_type:lower(), "dirichlet") ~= nil then
    str_w_bc = "Dirichlet_boundary([".. w_bc .."])"
elseif string.find(w_bc_type:lower(), "transmissive_momentum_set_stage") ~= nil then
    str_w_bc = "Transmissive_Momentum_Set_Stage_boundary(domain=domain, function=lambda t: ["
        .. w_bc .. "])"
end

f_driver:write(string.format(driver_file_template, str_resolution, str_interior_region, 
        n_boundary, e_boundary, s_boundary, w_boundary,
        remainder_res,
        str_qty,
        n_boundary, str_n_bc, 
        e_boundary, str_e_bc, 
        s_boundary, str_s_bc, 
        w_boundary, str_w_bc,
        '\n'))

f_driver:close( )