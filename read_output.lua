#!/usr/bin/env lua

-- read an ASCII bathymetry file from command prompt, (default: cairns.asc)
-- write a KML file denoting boundaries of the data, and the grid (default: grid.kml)
-- usage:  lua read_bathy.lua  infile  outfile.kml

fn_txt_output = "model_output.txt"
fn_kml = "model_output.kml"
CENTER_MERIDIAN = 147      -- zone 55
FALSE_EASTING = 500000
FALSE_NORTHING = 10000000
XLL = 301278.51     -- get from file extent
YLL = 7961069.14

-- Extract part of the triangular mesh around Cairns
X_CAIRNS = 145.775 -- 369704
Y_CAIRNS = 16.925 -- 8128724
RADIUS = 15000

-- USER INPUT

-- extent of visualization area
W_EXT = 145.14
E_EXT = 146.2
S_EXT = -17.7
N_EXT = -16.3

SIMUL_TIME = 6000

DEEPEST = 0.5        -- max. inundation depth
DEEPEST_COLOR = 0xff
SHALLOWEST = 0
SHALLOWEST_COLOR = 0x00

-- END USER INPUT

require( 'conversion' )
local f_txt = assert(io.open(fn_txt_output, "r"))
local f_kml = assert(io.open(fn_kml, "w"))

kml_header = [[<?xml version="1.0" encoding="utf-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Folder>
    <name>%s</name>
    <open>0</open>
    <description>%s</description>
]]

kml_triang_vol = [[
    <Placemark id="%d">
        <name>%f</name>
        <Style>
            <LineStyle>
                <color>%x</color> 
                <width>%f</width>
            </LineStyle>
            <PolyStyle>
                <color>%x</color>
            </PolyStyle>
        </Style>
            <Polygon>
                <extrude>0</extrude>
                <tessellate>0</tessellate>
                <altitudeMode>clampToGround</altitudeMode>
                <outerBoundaryIs>
                    <LinearRing>
                        <coordinates>
                            %f, %f 
                            %f, %f 
                            %f, %f 
                            %f, %f 
                        </coordinates>
                    </LinearRing>
                </outerBoundaryIs>
            </Polygon>
    </Placemark>
]]

kml_depth_pt = [[
    <Placemark id="%d">
        <name>%0.1f</name>
        <Style>
            <IconStyle>
                <color>%x</color>
                <scale>0.1</scale>
                <Icon>
                    <href>circle_1.png</href>
                </Icon>
            </IconStyle>
            <LabelStyle>
                <scale>0.0</scale>
            </LabelStyle>
        </Style>
        <Point>
            <coordinates>%f, %f</coordinates>
        </Point>
    </Placemark>
]]

kml_footer = [[
  </Folder>
</kml>
]]

readnum = function( )
    local num
    repeat
        local ch = f_txt:read( 1 )        -- read the comma or semicolon
        num = f_txt:read('*n')
    until num ~= nil
    return num
end

starttime = os.clock( )
-- read the dimensions variable from line 3 until line variables:
dimensions = {}
i = 0
repeat
    i = i + 1
    line = f_txt:read( )
    dimensions[i] = line
until (line == "variables:")

for i = 1, #dimensions do
    n_volumes = n_volumes or dimensions[i]:match('number_of_volumes%s*=%s*(%d+)')
    n_vertices = n_vertices or dimensions[i]:match('number_of_vertices%s*=%s*(%d+)')
    n_points = n_points or dimensions[i]:match('number_of_points%s*=%s*(%d+)')
    n_timesteps = n_timesteps or dimensions[i]:match('(%d+) currently')
end

repeat 
until f_txt:read( ) == "data:"

-- read n_points x values:
x = {}
for i = 1, n_points do
    x[i] = readnum( )
end

f_txt:read( )

-- read n_points y values:
y = {}
for i = 1, n_points do
    y[i] = readnum( )
end

f_txt:read( )

-- read n_points elevation values:
elevation = {}
for i = 1, n_points do
    elevation[i] = readnum( )
end

-- omitting z values
repeat
until (f_txt:read( ):match "volumes =")

-- now choose the points which lie within the extent specified
pt_selection = {}
k = 0
for i = 1, n_points do
    xp = x[i]
    yp = y[i]
    xp = xp + XLL - FALSE_EASTING
    yp = yp + YLL - FALSE_NORTHING
    longp, latp = long_lat_from_UTM( xp, yp, CENTER_MERIDIAN )
    if (W_EXT < longp) and (longp < E_EXT) and
        (S_EXT < latp) and (latp < N_EXT) then
            k = k + 1
            pt_selection[k] = {i, longp, latp}
    end
end

f_txt:read( )

time = {}
for i = 1, n_timesteps do
    time[i] = readnum( )
    print( time[i] )
end


repeat
until (f_txt:read( ):match " stage =" )

stage = {}
for i = 1, n_timesteps do
    stage[i] = {}
    for j = 1, n_points do
        stage[i][j] = readnum( )
    end
end

-- Draw the output stage
pos = 1     -- find index of time[] corresponding to SIMUL_TIME
while time[pos] < SIMUL_TIME do
    pos = pos + 1
end

repeat
until (f_txt:read( ):match "xmomentum =" ) 
xmomentum = {}
for i = 1, n_timesteps do
    xmomentum[i] = {}
    for j = 1, n_points do
        xmomentum[i][j] = readnum( )
    end
end

repeat
until (f_txt:read( ):match "ymomentum =" ) 
ymomentum = {}
for i = 1, n_timesteps do
    ymomentum[i] = {}
    for j = 1, n_points do
        ymomentum[i][j] = readnum( )
    end
end

f_kml:write( kml_header:format( "Inundation depth", "at time " .. SIMUL_TIME ) )

for j = 1, #pt_selection do
    k = pt_selection[j][1]
    dep = stage[pos][k] - stage[1][k]
    greenComp = (dep - SHALLOWEST) / (DEEPEST - SHALLOWEST) * 
            (DEEPEST_COLOR - SHALLOWEST_COLOR)
    if greenComp < 0 then greenComp = 0 end
    if greenComp > 0xff then greenComp = 0xff end
    color = 0xff00ffff - math.floor(greenComp) * 0x100
    f_kml:write( kml_depth_pt:format( j, dep, color, pt_selection[j][2], 
                    pt_selection[j][3]) )
                    
end

f_kml:write( kml_footer )
f_kml:close( )
f_txt:close( )
print( #pt_selection .. " points" )

fn_kmz = fn_kml:gsub("kml", "kmz")
os.execute("zip " .. fn_kmz .. " " .. fn_kml)

print( "Elapsed time:" .. os.clock( ) - starttime )
