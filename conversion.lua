-- Conversion from long lat to UTM and vice versa
-- Formulas are on website
-- http://www.uwgb.edu/dutchs/UsefulData/UTMFormulas.HTM

-- NO false easting / northing is accounted for.
-- you should manually add / subtract false easting / northing

-- trigonometric functions for angles in degrees
sind = function(x)
    return math.sin(x * math.pi / 180)
end

cosd = function(x)
    return math.cos(x * math.pi / 180)
end

tand = function(x)
    return math.tan(x * math.pi / 180)
end

local a = 6378137       -- equatorial radius
local b = 6356752.3142  -- polar radius
local k0 = 0.9996
local e = math.sqrt(1 - b^2 / a^2)
local ep2 = (e * a / b)^2
local n = (a - b) / (a + b)

UTM_from_long_lat = function(long, lat, long0)
    -- WGS84/NAD83 system
    -- long0 is the central meridian
    
    local rho = a * (1 - e^2) / (1 - e^2 * (sind(lat))^2)^1.5
    local nu = a / (1 - e^2 * (sind(lat))^2)^0.5
    local p = (long - long0) * math.pi / 180
    
    M = a * ( (1 - e^2 / 4 - 3*e^4 / 64 - 5 * e^6 / 256) * (lat * math.pi / 180)
            - (3 * e^2 / 8 + 3 * e^4 / 32 + 45 * e^6 / 1024) * sind(2*lat)
            + (15 * e^4 / 256 + 45 * e^6 / 1024) * sind(4*lat)
            - (35 * e^6 / 3072) * sind(6*lat) )
    K1 = M * k0
    K2 = k0 * nu * sind(lat) * cosd(lat) / 2
    K3 = (k0 * nu * sind(lat) * (cosd(lat))^3 / 24) * 
            (5 - (tand(lat))^2 + 9 * ep2 * (cosd(lat))^2 + 4 * ep2^2 * (cosd(lat))^4)
    K4 = k0 * nu * cosd(lat)
    K5 = (k0 * nu * (cosd(lat))^3 / 6) * (1 - (tand(lat))^2 + ep2 * (cosd(lat))^2)
    
    x = K4 * p + K5 * p^3
    y = K1 + K2*p^2 + K3*p^4
    
    return x, y
end

long_lat_from_UTM = function(x, y, lon_0)
    M = y / k0
    mu = M / (a * (1 - e^2 / 4 - 3 * e^4 / 64 - 5 * e^6 /  256))
    e1 = (1 - (1 - e^2)^0.5) / (1 + (1 - e^2)^0.5)
    
    J1 = 3 * e1 /2 - 27 * e1^3 / 32
    J2 = 21 * e1^2 / 16 - 55 * e1^4 / 32
    J3 = 151 * e1^3 / 96
    J4 = 1097 * e1^4 / 512
    sin = math.sin
    fp = mu + J1 * sin(2*mu) + J2 * sin(4*mu) + J3 * sin(6*mu) + J4 * sin(8*mu)
    C1 = ep2 * (math.cos(fp))^2
    T1 = (math.tan(fp))^2
    R1 = a * (1 - e^2) / (1 - e^2 * (sin(fp))^2)^1.5
    N1 = a / (1 - e^2 * (sin(fp))^2)^0.5
    D = x / (N1 * k0)
    
    Q1 = N1 * math.tan(fp) / R1
    Q2 = D^2 / 2
    Q3 = (5 + 3*T1 + 10*C1 - 4*C1^2 - 9*ep2) * D^4 / 24
    Q4 = (62 + 90*T1 + 298*C1 + 45*T1^2 - 3*C1^2 - 252*ep2) * D^6 / 720
    Q5 = D
    Q6 = (1 + 2 * T1 + C1 ) * D^3 / 6
    Q7 = (5 - 2 * C1 + 28 * T1 - 3 * C1^2 + 8 * ep2 + 24 * T1^2) * D^5 / 120
    
    lat = fp - Q1 * (Q2 - Q3 + Q4)
    lon = lon_0 + ((Q5 - Q6 + Q7) / math.cos(fp)) * 180 / math.pi
    return lon , lat * 180 / math.pi
end
